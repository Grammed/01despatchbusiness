
package current_version.o1model;

import current_version.o1model.hardenums.Status;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public abstract class ConsignmentHard extends ConsignmentProduct {
    private String unitNumber;      // Hard goods number within homeCCode e.g 01/24 todo add logic check only poss if consignment of 1
    private String productCode;     // Code of product the manufacturers product code for this particular item
    private String status;          // status of goods/ Working (has been checked) /working (supplier promised working) /unknown(not checked) not working (check failed see status note)
    private String statusNote;      // further info regarding status of item.  (interesting --> make another list with the status's linked to specific consignment/unitnumbers update test result)

    public ConsignmentHard(String uniqueID, String warehouse, String name, String datePurchased,
                           MinimumSaleUnit minimumSaleUnit, int noOfMinSaleUnits, float consignmentPrice,
                           float pricePerMinSaleUnit, String unitNumber, String productCode, String status,
                           String statusNote) {
        super(uniqueID, warehouse, name, datePurchased, minimumSaleUnit, noOfMinSaleUnits,
                consignmentPrice, pricePerMinSaleUnit);
       setUnitNumber(unitNumber);
       setProductCode(productCode);
       setStatus(status);
       setStatusNote(statusNote);
    }

    public void setUnitNumber(String unitNumber) {
        // todo see how this works, mainly for unit numbers and check status what about when bulk sales after checking individually?
        if ( super.getMinimumSaleUnit().label != 1 || super.getNoOfMinSaleUnits() != 1 ) // If either greater than one this is a consignment of more than 1 == needs further processing.
            this.unitNumber = "N/A_MultipleConsignment";
        this.unitNumber = unitNumber;
    }

    public void setProductCode(String productCode) {
        //Validation
        if ( super.getMinimumSaleUnit().label != 1 || super.getNoOfMinSaleUnits() != 1 )
            this.productCode = "N/A_MultipleConsignement";
        this.productCode = productCode;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setStatusNote(String statusNote) {
        this.statusNote = statusNote;
    }

    //This level getters
    public String getUnitNumber() {
        return unitNumber;
    }

    public String getProductCode() {
        return productCode;
    }

    public String getStatus() {
        return status;
    }

    public String getStatusNote() {
        return statusNote;
    }

    public Date getDatePurchased() {
        return super.getDatePurchased();
    }

    public String getStrDatePurchased(){
        return super.getStrDatePurchased();
    }
}
