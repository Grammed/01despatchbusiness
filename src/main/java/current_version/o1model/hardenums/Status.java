package current_version.o1model.hardenums;

public enum Status {
    WORKING("Tested by us"),
    WORKING_TRUSTED_SUPPLIER("Checked by supplier"),
    SUPPLIED_FOR_REPAIR("Supplied as not working"),
    UNKNOWN("Not tested, supplied as working"),
    REPAIR_OR_RETURN("Failed test");

    public final String LABEL;

    Status(String label) {
        this.LABEL = label;
    }
}
