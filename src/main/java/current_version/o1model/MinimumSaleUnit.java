package current_version.o1model;
//Minimum Sale Unit Quantity, very occasionally new formats will appear.
public enum MinimumSaleUnit {
    BOX_1(1),
    BOX_2(2),
    BOX_6(16),
    BOX_12(12),
    BOX_24(24),
    BOX_36(36),
    BOX_48(48),
    KG_1(1),
    KG_2(2),
    KG_4(4),
    KG_5(5),
    KG_8(8),
    KG_10(10),
    KG_NEW_AMOUNT(1),
    BOX_NEW_AMOUNT(1);

    public final int label;

    MinimumSaleUnit(int label) {
        this.label = label;
    }
}
