package current_version.o1model;

public enum ProductClass {
    C_APPLES("apples"),
    C_PEACHES("peaches"),
    C_LAPTOP("laptop"),
    C_MONITOR("monitor");

    public final String LABEL;

    ProductClass(String label) {
        this.LABEL = label;
    }
}