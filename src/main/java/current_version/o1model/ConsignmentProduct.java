package current_version.o1model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public abstract class ConsignmentProduct implements Packable {  // todo implement Shippable. Work out limitation of implementing Comparable here(all sub classes only comparable on warehouse.
    private SimpleDateFormat simpleDate = new SimpleDateFormat("yyyy/MM/dd"); //look at date getters to see how used to get back to string.
    private static int count;
    private static String defaultAwayCode = "AwayCodeUnknown", defaultNote = "Write info here...";
    private static String defaultDate = "1920/01/01";

    // ID
    private String uniqueID;
    // CProduct Required
    private String warehouse;       // Differentiates between Hard and Soft, but not 100% Misdelivery etc.
    private String name;            // Name of product // todo make this supplier have supplier enum that could become table.
    private Date datePurchased;     // Date of purchase
    private MinimumSaleUnit minimumSaleUnit;    // *1 The number of single items making up a sale/purchase unit. Default 1 for Hard goods and sometimes used for batch deals, More often used for fruits.
    private int noOfMinSaleUnits;        // todo test this and minimum sale unit == 1 allows for unit data to be added.*1 Total quantity of minimum sale units for this consignment.  Setters will set 1 kg 12 msu dependent on class.
    private float consignmentPrice; // *1 Full purchase price of the consignment. todo user action checkmsu price returns those that dont make sense.
    private float pricePerMinSaleUnit;        // *1 CONSIGNMENT PRICE / msuQuantity Hard goods == cost per msu minimumSaleUnit usually 1 // Soft goods = cost per msu or weight unit defined in msu.
    // todo method setSingleUnits = msu Quantity * minimumSale unit
    // CProduct Optional
    private String awayCCode;       // Vendor Consignment code / Customer Purchase Order Number
    private String note;            // Consignment note --> fill in with default 'OK' but used to store action.
    private Date dateReceived;      // Date of arrival in warehouse Can be null if has not arrived.
    private int singleUnits;      // unsellable in this unit normally but universal for reports.
    private float singleUnitPrice;  // consignment price / single units
    private boolean action;         // boolean null = no action false == ActionRequired true == Other option
    private float SalePrice;        // The actual sale price realised.
    // SOFT ONLY
    //private LocalDate sellByDate;   // Deadline for moving perishable stock on or changing storage medium.
    //private float averageWeight;
    //private Quality quality;
    //private Variety type;
    // HARD ONLY
    //private String unitNumber;      // Hard goods number within homeCCode e.g 01/24 todo  add later! this is only possible 1. if individual units are held in internal array. 2 if booked in one at a time
    //private String productCode;     // Code of product the manufacturers product code for this particular item // todo add later! only if individula units held in internal array
    //private float reqSalePrice;     // required sale price the sale price in mind at time of purchase
    //private String status;          // status of goods/ Working (has been checked) /working (supplier promised working) /unknown(not checked) not working (check failed see status note)
    //private String statusNote;      // further info regarding status of item.  (interesting --> make another list with the status's linked to specific consignment/unitnumbers update test result)

    {
        count++;
    }

    //Minimum required constructor
    public ConsignmentProduct(String uniqueID, String warehouse, String name, String datePurchased,
                              MinimumSaleUnit minimumSaleUnit, int noOfMinSaleUnits, float consignmentPrice,
                              float pricePerMinSaleUnit) {
        //Required fields
        setUniqueID(uniqueID);
        setWarehouse(warehouse);
        setName(name);
        setDatePurchased(datePurchased);
        setMinimumSaleUnit(minimumSaleUnit);
        setNoOfMinSaleUnits(noOfMinSaleUnits);
        setConsignmentPrice(consignmentPrice);
        setPricePerMinSaleUnit(pricePerMinSaleUnit);
        //Optional not primitive
        this.awayCCode = defaultAwayCode;
        this.note = defaultNote;
        setNotDeliveredYet(defaultDate); // setter parses default date 01011920
    }


    // START REQUIRED SETTERS THEN GETTERS
    public void setUniqueID(String uniqueID) {
        if (uniqueID == null || uniqueID.isBlank()) throw new IllegalArgumentException(
                "ConsignmentUniqueID cannot be null or blank");
        this.uniqueID = uniqueID;
    }

    public void setWarehouse(String warehouse) {
        if (warehouse == null || warehouse.isBlank()) throw new IllegalArgumentException(
                "ConsignmentWarehouse cannot be null or blank");
        this.warehouse = warehouse;
    }

    public void setName(String name) {
        if (name == null || name.isBlank()) throw new IllegalArgumentException(
                "ConsignmentName cannot be null or blank");
        this.name = name;
    }

    public void setDatePurchased(String datePurchased) {
        if (datePurchased == null || datePurchased.isBlank()) throw new IllegalArgumentException(
                "ConsignmentDatePurchased cannot be null or blank");
        try {
            this.datePurchased = simpleDate.parse(datePurchased);
        } catch (ParseException e) {
            System.out.println("Incorrect datePurchased format got through to setter");
            e.printStackTrace();
        }
    }



    public void setNoOfMinSaleUnits(int noOfMinSaleUnits) {
        if (noOfMinSaleUnits <= 0) throw new IllegalArgumentException("A consignment cannot consist of 0 or less" +
                "Minimum Sale Units");
        this.noOfMinSaleUnits = noOfMinSaleUnits;
    }

    public void setConsignmentPrice(float consignmentPrice) {
        if (consignmentPrice <= 0) throw new IllegalArgumentException("Price of consignment cannot be 0 or less");
        this.consignmentPrice = consignmentPrice;
    }

    public void setMinimumSaleUnit(MinimumSaleUnit minimumSaleUnit) // This is a decision based on each product
    {
        this.minimumSaleUnit = minimumSaleUnit;
    }

    public void setPricePerMinSaleUnit(float pricePerMinSaleUnit) {
        if (pricePerMinSaleUnit <= 0) throw new IllegalArgumentException("PricePer minimum sale unit of consignment" +
                " cannot be 0 or less");
        this.pricePerMinSaleUnit = pricePerMinSaleUnit;
    }



    //END SETTERS

    public String getUniqueID() {
        return uniqueID;
    }

    public String getWarehouse() {
        return warehouse;
    }

    public String getName() {
        return name;
    }

    public Date getDatePurchased() {
        return datePurchased;
    }

    public String getStrDatePurchased(){
        return simpleDate.format(this.getDatePurchased());
    }

    public int getNoOfMinSaleUnits() {
        return noOfMinSaleUnits;
    }

    public float getConsignmentPrice() {
        return consignmentPrice;
    }

    public MinimumSaleUnit getMinimumSaleUnit() {
        return minimumSaleUnit;
    }

    public float getPricePerMinSaleUnit() {
        return pricePerMinSaleUnit;
    }//END REQUIRED GETTERS and SETTERS

    // START OPTIONAL GETTERS and SETTERS

    public String getAwayCCode() {
        return awayCCode;
    }

    public String getNote() {
        return note;
    }

    public Date getDateReceived() {
        return dateReceived;
    }

    public int getSingleUnits() {
        return singleUnits;
    }

    public float getSingleUnitPrice() {
        return singleUnitPrice;
    }

    public boolean isAction() {
        return action;
    }

    public float getSalePrice() {
        return SalePrice;
    }

   public void setAwayCCode (String awayCCode){
        this.awayCCode = awayCCode;
   }

    public void setNote(String note) {
        this.note = note;
    }

    public void setDateReceived(String dateReceived) {
        //todo add additional validation date received cannot be greater than today's date.
        //Add default value if null or blank
        if (dateReceived == null || dateReceived.isBlank()) {
            try {
                this.dateReceived = simpleDate.parse("1900/07/07");
            } catch (ParseException pe) {
                System.out.println("Incorrect default date Received String");
            }
        }
        try {
            this.dateReceived = simpleDate.parse(dateReceived);
        } catch (ParseException e) {
            System.out.println("Incorrect dateReceived format got through to setter");
            e.printStackTrace();
        }
    }

    public void setSingleUnits(int singleUnits) {
        this.singleUnits = singleUnits;
    }

    public void setSingleUnitPrice(float singleUnitPrice) {
        this.singleUnitPrice = singleUnitPrice;
    }

    public void setAction(boolean action) {
        this.action = action;
    }

    public void setSalePrice(float salePrice) {
        SalePrice = salePrice;
    }

    // if a delivery date is not entered a placeholder date is used that can later be searched against.
    public void setNotDeliveredYet(String defaultDate) {
        try {
            this.dateReceived = simpleDate.parse(defaultDate);
        } catch (ParseException e) {
            System.out.println("There was an issue when setting dateReceived to default date value" + defaultDate);
            e.printStackTrace();
        }
    }


    @Override
    public String toString() {
        return "ConsignmentProduct{" +
                "uniqueID='" + uniqueID + '\'' +
                ", warehouse='" + warehouse + '\'' +
                ", name='" + name + '\'' +
                ", datePurchased=" + datePurchased +
                ", minimumSaleUnit=" + minimumSaleUnit +
                ", noOfMinSaleUnits='" + noOfMinSaleUnits + '\'' +
                ", consignmentPrice=" + consignmentPrice +
                ", msuPrice=" + pricePerMinSaleUnit +
                ", singleUnits=" + singleUnits +
                ", singleUnitPrice=" + singleUnitPrice +
                ", awayCCode='" + awayCCode + '\'' +
                ", note='" + note + '\'' +
                ", action=" + action +
                ", SalePrice=" + SalePrice +
                ", dateReceived=" + dateReceived +
                '}';
    }
}
