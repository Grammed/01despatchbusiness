package current_version.o1model;

public interface Packable extends Shippable {
    void pack(PackingContext packingContext);

}
