package current_version.o1model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static current_version.o1model.Warehouses.*;
import static current_version.o1model.MinimumSaleUnit.*;
import static current_version.o1model.ProductClass.*;
import static current_version.o1model.hardenums.Status.*;
import static current_version.o1model.softenums.AppleVariety.*;
import static current_version.o1model.softenums.Quality.*;

public class ConsignmentData {
    private static int count;
    //Data is originally created as a HashMap then converted to a TreeMap for imaginary customer reasons.
    private Map<String, ConsignmentProduct> consignmentProductMap;
    private List<ConsignmentProduct> consignmentProductList;

    // constructor is ArrayList in Class example
    public ConsignmentData() {consignmentProductMap = new HashMap<>();}

    // Takes an Id created by createUniqueID and a consignment product todo these may have to come ready made.
    private void registerNewDelivery(String id,ConsignmentProduct cp) {
        // Validation
        if (cp == null) throw new IllegalArgumentException("DeliveryData Error: cannot register null as a new Consignment");
        consignmentProductMap.put(id,cp);
    }

    // Boolean to give removal confirmation, maybe I don't remove but set as returned/entered in error.
    public boolean remove(String key) {
        // Validation ?? //todo why boolean investigate how this works
        //return consignmentDataHashMap.remove(key);
        return true;
    }

    // todo return a list with an interesting breakdown equiv of getPlantsByNameCasual

    public List<ConsignmentProduct> getConsignmentProductList(){
        //todo convert hashmap to list of parent objects and return.
        return consignmentProductList;
    }

    public int getConsignmentProductMapSize() {
        return consignmentProductMap.size();
    }



    // Used to create unique zero buffered ids for use as keys in HashMap and TreeMap
    // Why? to make things more complicated! So this is not just copy exercise.
    private String createUniqueID(String name, String warehouse){
        count++;
        String buffer = "0000000";
        //System.out.println(count); // used to confirm incrementation working as expected
        int temp = count;
        while(temp > 0) {
            temp /= 10;
            buffer = buffer.replaceFirst("0", "");
        }

        return  warehouse + "_" + buffer + count + "_" + name;
    }
    // Create test Consignments
    /*public ConsignmentApples(String uniqueID, String warehouse, String name, String datePurchased,
                             MinimumSaleUnit minimumSaleUnit, int noOfMinSaleUnits, float consignmentPrice,
                             float pricePerMinSaleUnit, String saleDeadline, Quality quality, AppleVariety variety) {
        super(uniqueID, warehouse, name, datePurchased, minimumSaleUnit, noOfMinSaleUnits, consignmentPrice, pricePerMinSaleUnit, saleDeadline, quality);
        this.variety = variety;
    }*/
    public void init() {
        ConsignmentApples s1 = new ConsignmentApples(PreConstructor.createUniqueID(C_APPLES.LABEL,SOUTHWARK_PERISHABLES.LABEL),
                SOUTHWARK_PERISHABLES.LABEL, "Apples for apple bobbing", "2021/01/11", BOX_24,
                10,100.45f,10.5f,"2021/01/21",EXCELLENT,JAZZ);
        ConsignmentApples s2 = new ConsignmentApples(PreConstructor.createUniqueID(C_APPLES.LABEL,SOUTHWARK_PERISHABLES.LABEL),
                SOUTHWARK_PERISHABLES.LABEL, "Apples for local fruit festival", "2021/01/24", BOX_24,
                10,100.45f,10.5f,"2021/02/11",PREMIUM,PINKLADY);
        ConsignmentApples s3 = new ConsignmentApples(PreConstructor.createUniqueID(C_APPLES.LABEL,SOUTHWARK_PERISHABLES.LABEL),
                SOUTHWARK_PERISHABLES.LABEL, "Apples for cider demo", "2021/01/19", BOX_24,
                10,100.45f,0.0f,"2021/01/31",NORMAL,HOKUTO);
        ConsignmentApples s4 = new ConsignmentApples(PreConstructor.createUniqueID(C_APPLES.LABEL,SOUTHWARK_PERISHABLES.LABEL),
                SOUTHWARK_PERISHABLES.LABEL, "Apples to for throwing competition", "2021/01/24", BOX_48,
                10,10.45f,00.0f,"2021/01/25",SPOILED, GRANNYSMITH);
        ConsignmentLaptops s5 = new ConsignmentLaptops(PreConstructor.createUniqueID(C_LAPTOP.LABEL,HAMPTON_HARD_GOODS.LABEL),
                HAMPTON_HARD_GOODS.LABEL,"A batch of 10 laptops that need to be processed for individual sale","2021/01/25",
                BOX_1,1,10_000,1000,"","",WORKING.LABEL,"",1500);
        ConsignmentLaptops s6 = new ConsignmentLaptops(PreConstructor.createUniqueID(C_LAPTOP.LABEL,HAMPTON_HARD_GOODS.LABEL),
                HAMPTON_HARD_GOODS.LABEL,"A single laptop bought as seen","2021/01/25",BOX_1,1,300,
                300,"01","HP1234",UNKNOWN.LABEL,"Testing on Wed 27th",400);

        registerNewDelivery(s1.getUniqueID(), s1);
        registerNewDelivery(s2.getUniqueID(), s2);
        registerNewDelivery(s3.getUniqueID(), s3);
        registerNewDelivery(s4.getUniqueID(), s4);
        registerNewDelivery(s5.getUniqueID(), s5);
        registerNewDelivery(s6.getUniqueID(), s6);

    }
}