package current_version.o1model;


import java.util.Date;

public class ConsignmentLaptops extends ConsignmentHard {
    private float reqSalePrice;     // required sale price the sale price in mind at time of purchase
    private static int count = 0;
    {count++;}

    public ConsignmentLaptops(String uniqueID, String warehouse, String name, String datePurchased,
                              MinimumSaleUnit minimumSaleUnit, int noOfMinSaleUnits, float consignmentPrice,
                              float pricePerMinSaleUnit, String unitNumber, String productCode,
                              String status, String statusNote, float reqSalePrice) {
        super(uniqueID, warehouse, name, datePurchased, minimumSaleUnit, noOfMinSaleUnits, consignmentPrice,
                pricePerMinSaleUnit, unitNumber, productCode, status, statusNote);
        setReqSalePrice(reqSalePrice);
    }

    public float getReqSalePrice() {
        return reqSalePrice;
    }

    public void setReqSalePrice(float reqSalePrice) {
        this.reqSalePrice = reqSalePrice;
    }
// REQUIRED SuperGetters for class level reporting setters probably to follow.. for class level adjustments. Depends.

    public String getUniqueID() {
        return super.getUniqueID();
    }

    public String getWarehouse() {
        return super.getWarehouse();
    }

    public String getName() {
        return super.getName();
    }

    public Date getDatePurchased() {
        return super.getDatePurchased();
    }

    public String getStrDatePurchased() {
        return super.getStrDatePurchased();
    }

    public int getNoOfMinSaleUnits() {
        return super.getNoOfMinSaleUnits();
    }

    public float getConsignmentPrice() {
        return super.getConsignmentPrice();
    }

    public MinimumSaleUnit getMinimumSaleUnit() {
        return super.getMinimumSaleUnit();
    }
    // OPTIONAL superGetters
    public String getAwayCCode() {
        return super.getAwayCCode();
    }

    public String getNote() {
        return super.getNote();
    }

    public Date getDateReceived() {
        return super.getDateReceived();
    }

    public int getSingleUnits() {
        return super.getSingleUnits();
    }

    public float getSingleUnitPrice() {
        return super.getSingleUnitPrice();
    }

    public boolean isAction() {
        return super.isAction();
    }

    public float getSalePrice() {
        return super.getSalePrice();
    }

    @Override
    public void pack(PackingContext packingContext) {

    }
    @Override
    public void ship(int method) {

    }
}
