package current_version.o1model;


import current_version.o1model.softenums.AppleVariety;
import current_version.o1model.softenums.Quality;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ConsignmentApples extends ConsignmentSoft implements Comparable<ConsignmentApples> {
    //todo would like this to become Variety: https://stackoverflow.com/questions/7296785/using-nested-enum-types-in-java
    private final SimpleDateFormat simpleDate = new SimpleDateFormat("yyyy/MM/dd");
    private AppleVariety variety;
    private static int count = 0;
    {count++;}

    public ConsignmentApples(String uniqueID, String warehouse, String name, String datePurchased,
                             MinimumSaleUnit minimumSaleUnit, int noOfMinSaleUnits, float consignmentPrice,
                             float pricePerMinSaleUnit, String saleDeadline, Quality quality, AppleVariety variety) {
        super(uniqueID, warehouse, name, datePurchased, minimumSaleUnit, noOfMinSaleUnits, consignmentPrice, pricePerMinSaleUnit, saleDeadline, quality);
        this.variety = variety;
    }

    public AppleVariety getVariety() {
        return variety;
    }

    public void setVariety(AppleVariety variety) {
        this.variety = variety;
    }
    // REQUIRED SuperGetters for class level reporting setters probably to follow.. for class level adjustments. Depends.

    public String getUniqueID() {
        return super.getUniqueID();
    }

    public String getWarehouse() {
        return super.getWarehouse();
    }

    public String getName() {
        return super.getName();
    }

    public Date getDatePurchased() {
        return super.getDatePurchased();
    }

    public String getStrDatePurchased(){
        return simpleDate.format(super.getDatePurchased());
    }

    public int getNoOfMinSaleUnits() {
        return super.getNoOfMinSaleUnits();
    }

    public float getConsignmentPrice() {
        return super.getConsignmentPrice();
    }

    public MinimumSaleUnit getMinimumSaleUnit() {
        return super.getMinimumSaleUnit();
    }
    // OPTIONAL superGetters
    public String getAwayCCode() {
        return super.getAwayCCode();
    }

    public String getNote() {
        return super.getNote();
    }

    public Date getDateReceived() {
        return super.getDateReceived();
    }

    public int getSingleUnits() {
        return super.getSingleUnits();
    }

    public float getSingleUnitPrice() {
        return super.getSingleUnitPrice();
    }

    public boolean isAction() {
        return super.isAction();
    }

    public float getSalePrice() {
        return super.getSalePrice();
    }

    @Override
    public int compareTo(ConsignmentApples o) {
        return this.getSaleDeadline().compareTo(o.getSaleDeadline());//fixme fixed?: 01 Added compare to @ Shippable and implemented
    }

    @Override
    public void pack(PackingContext packingContext) {

    }

    @Override
    public void ship(int method) {

    }

    @Override
    public String toString() {
        return  super.toString() +
                "ConsignmentApples{" +
                "variety=" + variety +
                '}';
    }
}
