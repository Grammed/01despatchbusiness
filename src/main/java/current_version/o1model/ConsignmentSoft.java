package current_version.o1model;

import current_version.o1model.softenums.Quality;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
public abstract class ConsignmentSoft extends ConsignmentProduct {
    private final SimpleDateFormat simpleDate = new SimpleDateFormat("yyyy/MM/dd"); //look at date getters to see how used to get back to string.
    private Date saleDeadline;
    private Quality quality;

    public ConsignmentSoft(String uniqueID, String warehouse, String name, String datePurchased,
                           MinimumSaleUnit minimumSaleUnit, int noOfMinSaleUnits,
                           float consignmentPrice, float pricePerMinSaleUnit, String saleDeadline, Quality quality) {
        super(uniqueID, warehouse, name, datePurchased, minimumSaleUnit, noOfMinSaleUnits, consignmentPrice, pricePerMinSaleUnit);
        setSaleDeadline(saleDeadline);
        setQuality(quality);

    }

    public Date getSaleDeadline() {
        return saleDeadline;
    }

    public void setSaleDeadline(String saleDeadline) {
            //todo add additional validation based on time allowed in warehouse dependent on soft product and supplier.
            //Add default value if null or blank
            if (saleDeadline == null || saleDeadline.isBlank()) {
                try {
                    this.saleDeadline = simpleDate.parse("1900/07/07");
                } catch (ParseException pe) {
                    System.out.println("Incorrect default date Received String");
                }
            }
            try {
                this.saleDeadline = simpleDate.parse(saleDeadline);
            } catch (ParseException e) {
                System.out.println("Incorrect dateReceived format got through to setter");
                e.printStackTrace();
            }
    }

    public Quality getQuality() {
        return quality;
    }

    public void setQuality(Quality quality) {
        this.quality = quality;
    }

    @Override
    public String toString() {

        return   super.toString() +
                "ConsignmentSoft{" +
                "simpleDate=" + simpleDate +
                ", saleDeadline=" + saleDeadline +
                ", quality=" + quality +
                '}';
    }
}
