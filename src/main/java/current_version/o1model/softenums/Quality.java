package current_version.o1model.softenums;

public enum Quality {
    LUXURY,
    PREMIUM,
    EXCELLENT,
    GOOD,
    NORMAL,
    SPOILED
}
