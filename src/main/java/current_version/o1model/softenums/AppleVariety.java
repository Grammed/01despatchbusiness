package current_version.o1model.softenums;

public enum AppleVariety {
    BRAEBURN,
    GRANNYSMITH,
    HOKUTO,
    HONEYCRISP,
    JAZZ,
    PINKLADY,
    RUBYFROST,
    SUNDANCE,
}
