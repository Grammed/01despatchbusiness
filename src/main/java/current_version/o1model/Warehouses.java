package current_version.o1model;

public enum Warehouses {
    SOUTHWARK_PERISHABLES("#P"),
    HAMPTON_HARD_GOODS("#H");
    public final String LABEL;

    Warehouses(String label) {
        this.LABEL = label;
    }
}
