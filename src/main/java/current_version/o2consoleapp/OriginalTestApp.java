package current_version.o2consoleapp;


import current_version.o1model.*;

import java.util.*;
import java.util.stream.Collectors;

import static current_version.o1model.MinimumSaleUnit.*;
import static current_version.o1model.ProductClass.*;
import static current_version.o1model.Warehouses.*;;
import static current_version.o1model.softenums.AppleVariety.*;
import static current_version.o1model.softenums.Quality.*;
import static current_version.o1model.hardenums.Status.*;

/*
* Make classes sensibly polymorphic, at present there are not enough shared characteristics to make it worthwhile.
* Add all characteristics --> get from old version.
* Start on menus.*/
public class OriginalTestApp {

    public static void main(String[] args) {

        TreeMap<String, ConsignmentProduct> map;

        map = init();
        // iterate to all the keys stored on our hashmap

        for (String s : map.keySet()) { // #1. iterate to all the keys stored on oubr hashmap
            if(s.contains("apple")) {
                ConsignmentApples t = (ConsignmentApples) map.get(s); // without casting subClass UID not available.
                System.out.println(t.getUniqueID());
                //System.out.println(t.getStrDateOfPurchase()); todo add return str version of getDateOfPurchase
            }else {
                ConsignmentLaptops t = (ConsignmentLaptops) map.get(s); // plus this has to be added for
                System.out.println(t.getUniqueID());
                System.out.println(t.getStrDatePurchased());
            }
        }
        System.out.println("\n************HashMapExamples*************");
        System.out.println("Iterating original HashMap mkey.getValue() prints whole object!");
        for(Map.Entry<String, ConsignmentProduct> mkey: map.entrySet()){
            System.out.println("Key : " + mkey.getKey() + "\t\t"
                    + "Value : " + mkey.getValue());
        }

//fixme fixed: convert TreeMap to ArrayList: TM for speed converted to AL for ease of filtering.
        Collection<ConsignmentProduct> values = map.values();
        Collection<ConsignmentProduct> valuesList = new HashSet<ConsignmentProduct>(values);
        // todo put all shared characteristics at highest level == ConsignementProduct
        // Define different chars of subclasses.

//fixme fixed: showing how to get an Array List of only the class you want from a TreeMap
        ArrayList<ConsignmentProduct> appleConsignmentsOnly = map.values().stream()
                    .filter(objValueFromMap -> (objValueFromMap.getClass() == ConsignmentApples.class))
                    .collect(Collectors.toCollection(ArrayList<ConsignmentProduct>::new));

        System.out.println("\nPrinting after filtering values for class type rather than text search and creating AList<>:");
        appleConsignmentsOnly.stream().forEach(System.out::println);


        System.out.println("\nThis is not tested until two fruits exist, tried to do this with variable and {} failed");
        ArrayList<ConsignmentProduct> fruitConsignmentsOnly = map.values().stream()
                .filter(objValueFromMap ->
                    ((objValueFromMap.getClass() == ConsignmentApples.class)
                            || (objValueFromMap.getClass() == ConsignmentLaptops.class)))
                .collect(Collectors.toCollection(ArrayList<ConsignmentProduct>::new));

        System.out.println("\nThis will print everything but will be used just for fruits or hard goods in the future.");

        fruitConsignmentsOnly.forEach(System.out::println);

    }

    private static TreeMap<String, ConsignmentProduct> init() {
        ConsignmentApples s1 = new ConsignmentApples(PreConstructor.createUniqueID(C_APPLES.LABEL,SOUTHWARK_PERISHABLES.LABEL),
                SOUTHWARK_PERISHABLES.LABEL, "Apples for apple bobbing", "2021/01/11", BOX_24,
                10,100.45f,10.5f,"2021/01/21",EXCELLENT,JAZZ);
        ConsignmentApples s2 = new ConsignmentApples(PreConstructor.createUniqueID(C_APPLES.LABEL,SOUTHWARK_PERISHABLES.LABEL),
                SOUTHWARK_PERISHABLES.LABEL, "Apples for local fruit festival", "2021/01/24", BOX_24,
                10,100.45f,10.5f,"2021/02/11",PREMIUM,PINKLADY);
        ConsignmentApples s3 = new ConsignmentApples(PreConstructor.createUniqueID(C_APPLES.LABEL,SOUTHWARK_PERISHABLES.LABEL),
                SOUTHWARK_PERISHABLES.LABEL, "Apples for cider demo", "2021/01/19", BOX_24,
                10,100.45f,0.1f,"2021/01/31",NORMAL,HOKUTO);
        ConsignmentApples s4 = new ConsignmentApples(PreConstructor.createUniqueID(C_APPLES.LABEL,SOUTHWARK_PERISHABLES.LABEL),
                SOUTHWARK_PERISHABLES.LABEL, "Apples to for throwing competition", "2021/01/24", BOX_48,
                10,10.45f,00.2f,"2021/01/25",SPOILED, GRANNYSMITH);
        ConsignmentLaptops s5 = new ConsignmentLaptops(PreConstructor.createUniqueID(C_LAPTOP.LABEL,HAMPTON_HARD_GOODS.LABEL),
                HAMPTON_HARD_GOODS.LABEL,"A batch of 10 laptops that need to be processed for individual sale","2021/01/25",
                BOX_1,1,10_000,1000,"","",WORKING.LABEL,"",1500);
        ConsignmentLaptops s6 = new ConsignmentLaptops(PreConstructor.createUniqueID(C_LAPTOP.LABEL,HAMPTON_HARD_GOODS.LABEL),
                HAMPTON_HARD_GOODS.LABEL,"A single laptop bought as seen","2021/01/25",BOX_1,1,300,
                300,"01","HP1234",UNKNOWN.LABEL,"Testing on Wed 27th",400);

        // initialize TreeMap
        TreeMap<String, ConsignmentProduct> map = new TreeMap<>();

        // assign each Consignment Product id as key and the Consignment objects as values in
        // our TreeMap
        map.put(s1.getUniqueID(), s1);
        map.put(s2.getUniqueID(), s2);
        map.put(s3.getUniqueID(), s3);
        map.put(s4.getUniqueID(), s4);
        map.put(s5.getUniqueID(), s5);
        map.put(s6.getUniqueID(), s6);

        return map;
    }
}