package current_version.o2consoleapp;

import current_version.o1model.ConsignmentData;

public class App {
    public static void main(String[] args) {
        App app = new App();
        app.startMenu();

    }

    public void startMenu() {
        ConsignmentData dataNowLive = new ConsignmentData();
        dataNowLive.init();
        System.out.println("The size of HashMap is now " + dataNowLive.getConsignmentProductMapSize());
        greetings();
    }
    private void greetings() {
        System.out.println("Welcome to a very early version of this app");
    }

}
