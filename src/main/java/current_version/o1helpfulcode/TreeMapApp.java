package current_version.o1helpfulcode;

import java.util.Comparator;
import java.util.TreeMap;
import java.util.function.Predicate;

public class TreeMapApp {
    public static void main(String[] args) {
        System.out.println("\n** ages **");
        Comparator<String> c1 = (k1, k2) -> k1.length() - k2.length();
        Comparator<String> c2 = Comparator.naturalOrder();
        Comparator<String> c3 = c1.thenComparing(c2);
        Predicate<String> p1 = (s) -> s.contains("m");
        Predicate<String> p2 = (s) -> s.length() > 4;
        Predicate<String> p3 = p1.and(p2);
        TreeMap<String, Integer> map = new TreeMap<>(c3);//((k1, k2) -> k1.length() - k2.length());
        map.put("Truyen Ward", 36);
        map.put("Bruce Wayne", 40);
        map.put("Maria Magdalena", 24);
        map.put("Anne Frank", 16);

        var age = map.get("Truyen Ward");
        System.out.println("Age of Bruce Wayne: " + map.get("Bruce Wayne"));

        //# afprinten keys optie 1:
        System.out.println("\n** list of keys **");
        for( String key : map.keySet() ){
            System.out.println(key);
        }
        //# afprinten keys optie 2:
        System.out.println("\n** list of keys2 **");
        map.forEach((k,v) -> System.out.println(k));

        //# afprinten keys en values optie 1:
        System.out.println("\n** list of values **");
        for( String key : map.keySet() ){
            System.out.println("key: " + key + " -> " + map.get(key));
        }

        //# afprinten keys en values optie 2:
        System.out.println("\n** list of values2 **");
        map.forEach((k,v) -> System.out.println("key: " + k + " --> " + v));

        System.out.println("\n** ingredients **");
        TreeMap<String, String> ingredients = new TreeMap<>();
        ingredients.put("bloem", "100 g");
        ingredients.put("melk", "0,5 l");
        ingredients.put("eieren", "2 stuks");
        ingredients.put("suiker", "100 g");

        var value = ingredients.get("eieren");

        //# afprinten keys en values:
        System.out.println("\n** list of ingredients **");
        ingredients.forEach((k, v) -> System.out.println("key: " + k + " -> " + v));

        //# afprinten keys en values met filter:
        System.out.println("\n** filtered list of ingredients **");
        ingredients.keySet().stream().filter(p3).forEach((k) -> System.out.println("key: " + k + " -> " + ingredients.get(k)));
    }
}
